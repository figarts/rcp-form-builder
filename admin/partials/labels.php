<?php
/**
 * Labels
 *
 * Shows the form for selected labels
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
?>

<h2><?php esc_html_e('RCP Labels', 'rcpfb'); ?></h2>
<p><?php esc_html_e('Leave blank to show default labels', 'rcpfb'); ?></p>
<table class="form-table">

  <tr valign="top">
    <th scop="row">
      <label for="logged_out_header"><?php esc_html_e( 'Logged Out Header', 'rpcfb' ); ?></label>
    </th>
    <td>
      <input class="regular-text" type="text" id="rcpfb_labels[logged_out_header]" style="width: 300px;" name="rcpfb_labels[logged_out_header]" value="<?php if( isset( $labels['logged_out_header'] ) ) { echo esc_attr( $labels['logged_out_header'] ); } ?>"/>
      <p class="description"><?php esc_html_e( 'Default: Register New Account', 'rpcfb'); ?></p>
    </td>
  </tr>

  <tr valign="top">
    <th scop="row">
      <label for="logged_in_header"><?php esc_html_e( 'Logged In Header', 'rpcfb' ); ?></label>
    </th>
    <td>
      <input class="regular-text" type="text" id="rcpfb_labels[logged_in_header]" style="width: 300px;" name="rcpfb_labels[logged_in_header]" value="<?php if( isset( $labels['logged_in_header'] ) ) { echo esc_attr( $labels['logged_in_header'] ); } ?>"/>
      <p class="description"><?php esc_html_e( 'Default: Upgrade or Renew Your Subscription', 'rpcfb'); ?></p>
    </td>
  </tr>

  <tr valign="top">
    <th scop="row">
      <label for="rcp_sub_details_renew"><?php esc_html_e( 'Registered Message', 'rpcfb' ); ?></label>
    </th>
    <td>
      <input class="regular-text" type="text" id="rcpfb_labels[rcp_sub_details_renew]" style="width: 300px;" name="rcpfb_labels[rcp_sub_details_renew]" value="<?php if( isset( $labels['rcp_sub_details_renew'] ) ) { echo esc_attr( $labels['rcp_sub_details_renew'] ); } ?>"/>
      <p class="description"><?php esc_html_e( 'Default: Renew your subscription', 'rpcfb'); ?></p>
    </td>
  </tr>
  
  <tr valign="top">
    <th scop="row">
      <label for="rcp_subscription_message"><?php esc_html_e( 'Subscription Message', 'rpcfb' ); ?></label>
    </th>
    <td>
      <input class="regular-text" type="text" id="rcpfb_labels[rcp_subscription_message]" style="width: 300px;" name="rcpfb_labels[rcp_subscription_message]" value="<?php if( isset( $labels['rcp_subscription_message'] ) ) { echo esc_attr( $labels['rcp_subscription_message'] ); } ?>"/>
      <p class="description"><?php esc_html_e( 'Default: Choose your subscription level', 'rpcfb'); ?></p>
    </td>
  </tr>
  
  <tr valign="top">
    <th scop="row">
      <label for="rcp_submit_registration"><?php esc_html_e( 'Register Button', 'rpcfb' ); ?></label>
    </th>
    <td>
      <input class="regular-text" type="text" id="rcpfb_labels[rcp_submit_registration]" style="width: 300px;" name="rcpfb_labels[rcp_submit_registration]" value="<?php if( isset( $labels['rcp_submit_registration'] ) ) { echo esc_attr( $labels['rcp_submit_registration'] ); } ?>"/>
      <p class="description"><?php esc_html_e( 'Default: Register', 'rpcfb'); ?></p>
    </td>
  </tr>
</table>