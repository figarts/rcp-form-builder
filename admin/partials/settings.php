<?php
/**
 * Settings
 *
 * Shows the form import/export forms 
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
?>

<h2><?php screen_icon(); esc_html_e('Settings', 'rcpfb'); ?></h2>
<div class="metabox-holder">
  <div class="postbox">
    <h3><span><?php esc_html_e( 'Export Settings', 'rcpfb' ); ?></span></h3>
    <div class="inside">
      <p><?php esc_html_e( 'Export the form fields as a .json file. This allows you to easily import the form into another site', 'rcpfb' ); ?></p>                   
      <a href="<?php echo esc_url( wp_nonce_url(admin_url('admin.php?page=rcp_form_builder&rcpfb_export=1'), 'exporting_rcpfb', 'rcpfb_nonce') ); ?>" class="button secondary-button"><?php esc_html_e( 'Export', 'rcpfb' ) ?> </a>
    </div><!-- .inside -->
  </div><!-- .postbox -->

  <div class="postbox">
    <h3><span><?php _e( 'Import Settings' ); ?></span></h3>
    <div class="inside">
      <p><?php esc_html_e( 'Import the form fields from a .json file. This file can be obtained by exporting the form on another site using the form above.' ); ?></p>
      <p>
        <input type="file" name="rcpfb_import_file"/>
      </p>
    </div><!-- .inside -->
  </div><!-- .postbox -->

  <div class="postbox">
    <h3><span><?php _e( 'Reset Form' ); ?></span></h3>
    <div class="inside">
      <p><?php esc_html_e( 'Resetting form will delete fields, checkouts and settings created with this plugin.' ); ?></p>
       <a onclick="return confirm('Are you sure you want to reset form?');" href="<?php echo esc_url( wp_nonce_url(admin_url('admin.php?page=rcp_form_builder&rcpfb_reset=1'), 'resets_rcpfb', 'rcpfb_nonce') ); ?>" class="button rcpfb--button"><?php esc_html_e( 'Reset', 'rcpfb' ) ?> </a>
    </div><!-- .inside -->
  </div><!-- .postbox -->


</div><!-- .metabox-holder -->  