<?php
/**
 * Builder
 *
 * Shows the form builder 
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
?>
<script>

(function( $ ) {
	'use strict';

	 $(function () {
    var fbTemplate = document.getElementById('rcpfb-form-fields'); 
    var fields = [
      {
        label: "HTML",
        type: "paragraph",
        // subtype: "html",
        icon: "&#10100; &#10101;"
      }
    ];  
    var options = {
      disableFields: ['button','autocomplete','file','number','paragraph', 'header'],
      controlOrder: ['text','textarea','checkbox-group','radio-group','select','date'],    
      // disabledActionButtons: ['data','clear','save'],
      disabledAttrs: ['toggle','placeholder','inline','other',],      
      typeUserDisabledAttrs: {
        'text': [  'value',],
        'hidden': [  'memberslistcsv',  'access'],
        'textarea': [  'value',  'subtype'],
        'file' : [  'multiple',  'subtype'],
        'number' : [  'min',  'max',  'step'],         
      },
      typeUserEvents: {
        text: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        },
        textarea: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        'checkbox-group': {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        },
        'radio-group': {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        select: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        date: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        hidden: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
       file: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        header: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        }, 
        paragraph: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld);
          }
        },   
      },      
      typeUserAttrs: rcpfbJS.type_attr,
      roles: <?php echo rcpfb_get_rcplevels() ?>,
      formData: '<?php echo $fields ?>',  
      i18n: {
        locale: rcpfbJS.locale.replace(/_/g, '-'),
        location: rcpfbJS.url
        //extension: '.ext'
        //preloaded: {
        //    'en-US': {...}
        //}
      },
      fields: fields
    };
    var formBuilder = $(fbTemplate).formBuilder(options);
    $('#rcpfb-form').submit(function(e) {       
      e.preventDefault(); 
      var data = formBuilder.actions.getData('json', true);
      $('#rcpfb-form-data').val(data);
      $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
    }); 


    /**
     * Fix Custom Checked Box Form Builder.
     * 
     * @param {type} fld
     * @returns {undefined}
     */
    function rcpfbFixCheckedPropForField (fld, fieldNames) {
      var optionFields = ['checkbox', 'radio-group', 'select'];     
      if ( jQuery.inArray( $(fld).attr('type'), optionFields ) !== -1 ) {
        $( $(fld).find('.sortable-options li input[type="radio"]') ).each( function( index, element ){
          if ($( this ).val() == '1') {
            $( this ).attr('checked', 'checked');
          }
        });
      }
    };
	});

})( jQuery );

</script>
