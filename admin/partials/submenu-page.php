<?php
/**
 * Provide a admin area view for the plugin
 *
 * @link       https://figarts.co
 * @since      1.0.0
 *
 * @package    Rcpfb
 * @subpackage Rcpfb/admin/partials
 */
?>

<div id="wp-body">
  <div id="wpbody-content">

    <div class="wrap">
      <?php printf('<h1>%s</h1>', esc_html__('Restrict Content Pro - Fields', 'rcpfb')) ?>	
      <div id="tabs" class="rcpfb-tabs no-js">


        <form method="post" action="" id="rcpfb-form" enctype="multipart/form-data">

	        <h2 class="nav-tab-wrapper" style="margin-bottom: 0px">
	          <a href="?page=rcpfb&tab=display_fields" class="nav-tab is-active">
	          	<?php esc_html_e('Form Fields', 'rcpfb') ?>	
	          </a>
	          <a href="?page=rcpfb&tab=display_labels" class="nav-tab">
	          	<?php esc_html_e('Form Labels', 'rcpfb') ?>	
	          </a>
	          <a href="?page=rcpfb&tab=display_settings" class="nav-tab">
	          	<?php esc_html_e('Settings', 'rcpfb') ?>	
	          </a>
	        </h2>

	        <div class="rcpfb-tab is-active">
	          <div id="rcpfb-form-fields" class="rcpfb-tab__content"></div>
						<input type="hidden" name="rcpfb_form_data" id="rcpfb-form-data">
            <?php //require_once RCPFB_ADMIN_PARTIALS . '/builder.php'; ?>
					</div>

	        <div class="rcpfb-tab">
	          <div id="rcpfb-form-fields" class="rcpfb-tab__content"></div>
            <?php require_once RCPFB_ADMIN_PARTIALS . '/labels.php'; ?>
					</div>

	        <div class="rcpfb-tab">
	          <div id="rcpfb-form-fields" class="rcpfb-tab__content"></div>
            <?php require_once RCPFB_ADMIN_PARTIALS . '/settings.php'; ?>
					</div>

        <?php 
          wp_nonce_field( 'rcpfb_save_nonce' );
          submit_button( esc_html__('Save Form', 'rcpfb'), 'rcpfb-button primary', 'save_rcpfb' );
        ?>

        </form>


      </div><!--tabs-->
    </div><!--wrap-->
  </div><!--wpbody-content-->
</div><!--wp-body-->