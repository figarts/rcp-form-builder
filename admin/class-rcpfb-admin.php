<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Rcpfb_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->initialize();

	}

	/**
	 * Registers plugin options
	 *
	 * @since    1.0.0
	 */
	function initialize(){
		// delete_option('rcpfb');
		if( false == get_option( 'rcpfb' ) ) {  
		  add_option( 'rcpfb', rcpfb_default_options() );
    } // end if
    

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
    $screen = get_current_screen();
       
		if (stripos( $screen->id, 'rcp_form_builder') !== false) {
      wp_enqueue_style( 'rcpfb-select2', RCPFB_DIR_URL . 'assets/css/select2.min.css', array(), $this->version, 'all' );
      wp_enqueue_style( $this->plugin_name, RCPFB_DIR_URL . 'admin/css/rcpfb-admin.css', array(), $this->version, 'all' );
    }

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		$screen = get_current_screen();
        
		if (stripos( $screen->id, 'rcp_form_builder') !== false) {
      wp_enqueue_script( 'rcpfb-fb', RCPFB_DIR_URL . 'admin/js/form-builder.min.js', array( 'jquery', 'jquery-ui-sortable' ), $this->version, true );
      wp_enqueue_script( 'rcpfb-tabs', RCPFB_DIR_URL . 'admin/js/tabs.js', array( 'jquery' ), $this->version, true );
      wp_enqueue_script( $this->plugin_name, RCPFB_DIR_URL . 'admin/js/rcpfb-admin.js', array( 'jquery' ), $this->version, true );
		  wp_enqueue_script( 'rcpfb-select2', RCPFB_DIR_URL . 'assets/js/select2.min.js', array( 'jquery' ), $this->version, true );

			$data = get_option('rcpfb');
			$data = json_encode($data['fields']);
      // Localize the script to send locale to form builder
      $translation_array = array(
        'url' => RCPFB_DIR_URL . 'languages/',
        'locale' => in_array( get_locale(), rcpfb_formbuilder_languages() ) ? get_locale() : 'en_US',
        'type_attr' => rcpfb_get_type_attr(),
        'data' => $data,
        'roles' => rcpfb_get_rcplevels(false),
      );
      wp_localize_script( 'rcpfb-fb', 'rcpfbJS', $translation_array );
    }


  }

	/**
	 * Adds a submenu under Restrict menu
	 *
	 * @since    1.0.0
	 */
	public function add_submenu() {
	  add_submenu_page(
  		'rcp-members',
      esc_html__( 'Fields', 'rcpfb' ),  // The title
      esc_html__( 'Fields', 'rcpfb' ),  // The text to be displayed for this menu item
      'manage_options',                       // Which type of users can see this menu item
      'rcp_form_builder',                     // The unique ID - that is, the slug - for this menu item
      array( $this, 'render_submenu_page' )   // The name of the function to call 
	  );
	} 

	/**
	 * Renders the submenu created above
	 *
	 * @since    1.0.0
	 */
	function render_submenu_page() {

    $data = get_option('rcpfb');
    $fields = rcpfb_array_to_object($data['fields']);
    // wp_dump($fields);    
    $labels = $data['labels'];
		require_once RCPFB_ADMIN_PARTIALS . '/submenu-page.php';
	} 



	/**
	 * Saves form settings
	 *
	 * @since    1.0.0
	 */
	public function save_settings(){
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['rcpfb_form_data'])) {
      
      if( ! current_user_can( 'manage_options' ) )
        return;

      // Do a nonce security check
      $nonce = $_REQUEST['_wpnonce'];
      if ( ! wp_verify_nonce( $nonce, 'rcpfb_save_nonce' ) ) wp_die( 'Security check' ); 
      $data = [];  
      
      if( ! empty( $_FILES['rcpfb_import_file']['name']) ){

        $name = $_FILES['rcpfb_import_file']['name'];
        $extension = end( explode( '.', $name ) );

        if( $extension != 'json' ) {
          wp_die( __( 'Please upload a valid .json file' ) );
        }

        $import_file = $_FILES['rcpfb_import_file']['tmp_name'];
        if( empty( $import_file ) ) {
          wp_die( __( 'Please upload a file to import' ) );
        }

        // Retrieve the form from the file and convert the json object to an array.
        $posted_fields = file_get_contents( $import_file );
        $posted_fields = trim($posted_fields, '"');
      }
      else{
        $posted_fields = $_POST['rcpfb_form_data'];
      }
      // wp_dump($posted_fields);
      $fields = stripslashes( wp_kses_post($posted_fields) );
      $data['fields'] = json_decode( $fields, TRUE);

      if ( isset($_POST['rcpfb_labels']) && is_array($_POST['rcpfb_labels']) ) {
        foreach ($_POST['rcpfb_labels'] as $key => $value) {
          $data['labels'][$key] = sanitize_text_field( $value );
        }
      }else{
        $data['labels'] = array();
      }
      update_option('rcpfb', $data);
    } 
	}



	/**
	 * Process Form Export
	 *
	 * @since    1.0.0
	 */
  function process_form_export() {

    if( empty( $_GET['rcpfb_export'] ) || '1' != $_GET['rcpfb_export'] )
      return;

    if (!isset($_GET['rcpfb_nonce']) || !wp_verify_nonce($_GET['rcpfb_nonce'], 'exporting_rcpfb'))
      return;

    if( ! current_user_can( 'manage_options' ) )
      return;

    $data = get_option('rcpfb');
    $fields = rcpfb_array_to_object($data['fields']);

    ignore_user_abort( true );

    nocache_headers();
    header( 'Content-Type: application/json; charset=utf-8' );
    header( 'Content-Disposition: attachment; filename=rcpfb-fields-' . date( 'm-d-Y' ) . '.json' );
    header( "Expires: 0" );

    echo json_encode( $fields );
    exit;
  }

	/**
	 * Reset Form
	 *
	 * @since    1.0.0
	 */
  function reset_form() {

    if( empty( $_GET['rcpfb_reset'] ) || '1' != $_GET['rcpfb_reset'] )
      return;

    if (!isset($_GET['rcpfb_nonce']) || !wp_verify_nonce($_GET['rcpfb_nonce'], 'resets_rcpfb'))
      return;

    if( ! current_user_can( 'manage_options' ) )
      return;

		delete_option('rcpfb');
		wp_safe_redirect( admin_url('admin.php?page=rcp_form_builder') ); exit;
  }

}
