(function ($) {
  'use strict';
	/**
	 * All of the code for admin-facing JavaScript source
	 * reside in this file.
	 */
  var fieldNames = ['invoice', 'memberslistcsv', 'hidefromloggedin'];

  $(function () {
    var rcpfbTabs = tabs({
      el: '.rcpfb-tabs',
      tabNavigationLinks: '.nav-tab',
      tabContentContainers: '.rcpfb-tab'
    });
    rcpfbTabs.init();
  });


  $(function () {
    var fbTemplate = document.getElementById('rcpfb-form-fields');
    var fields = [
      {
        label: "HTML",
        type: "paragraph",
        // subtype: "html",
        icon: "&#10100; &#10101;"
      }
    ];
    var options = {
      disableFields: ['button', 'autocomplete', 'file', 'number', 'paragraph', 'header'],
      controlOrder: ['text', 'textarea', 'checkbox-group', 'radio-group', 'select', 'date'],
      disabledActionButtons: ['data', 'clear', 'save'],
      disabledAttrs: ['toggle', 'placeholder', 'inline', 'other',],
      typeUserDisabledAttrs: {
        'text': ['value',],
        'hidden': ['memberslistcsv', 'access'],
        'textarea': ['value', 'subtype'],
        'file': ['multiple', 'subtype'],
        'number': ['min', 'max', 'step'],
      },
      typeUserEvents: {
        text: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
        textarea: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames, fieldNames);
          }
        },
        'checkbox-group': {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames, fieldNames);
          }
        },
        'radio-group': {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames, fieldNames);
          }
        },
        select: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames, fieldNames);
          }
        },
        date: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
        hidden: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
        file: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
        header: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
        paragraph: {
          onadd: function (fld) {
            rcpfbFixCheckedPropForField(fld, fieldNames);
          }
        },
      },
      typeUserAttrs: rcpfbJS.type_attr,
      roles: rcpfbJS.roles,
      formData: rcpfbJS.data,
      i18n: {
        locale: rcpfbJS.locale.replace(/_/g, '-'),
        location: rcpfbJS.url
        //extension: '.ext'
        //preloaded: {
        //    'en-US': {...}
        //}
      },
      fields: fields
    };
    var formBuilder = $(fbTemplate).formBuilder(options);
    $('#rcpfb-form').submit(function (e) {
      e.preventDefault();
      var data = formBuilder.actions.getData('json', true);
      $('#rcpfb-form-data').val(data);
      $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
    });


    /**
     * Fix Custom Checked Box Form Builder.
     * 
     * @param {type} fld
     * @returns {undefined}
     */
    function rcpfbFixCheckedPropForField(fld, fieldNames) {

      $.each(fieldNames, function (index, value) {
        var $checkbox = $(".fld-" + value, fld);
        // According to the value of the attribute "value", check or uncheck
        if ($checkbox.val()) {
          $checkbox.attr("checked", true);
        } else {
          $checkbox.attr("checked", false);
        }
      });

      var optionFields = ['checkbox', 'radio-group', 'select'];
      if (jQuery.inArray($(fld).attr('type'), optionFields) !== -1) {
        $($(fld).find('.sortable-options li input[type="radio"]')).each(function (index, element) {
          if ($(this).val() == '1') {
            $(this).attr('checked', 'checked');
          }
        });
      }
    };


  });





})(jQuery);
