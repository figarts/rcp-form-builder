(function ($) {
  'use strict';
	/**
	 * All of the code for admin-facing JavaScript source
	 * reside in this file.
	 */

  $(function () {
    rcpfb_toggle_fields();
    $('body').on('rcp_discount_change rcp_level_change rcp_gateway_change', rcpfb_toggle_fields);

  });


  function rcpfb_toggle_fields() {

    var request;
    var form = $('#rcp_registration_form');
    var values = form.serializeArray();
    var skip = ['rcp_register_nonce', 'rcp_user_pass', 'rcp_user_pass_confirm', 'rcp_card_number'];
    var data = {
      action: 'rcpfb_toggle_fields'
    };


    if (!form.length) {
      return;
    }

    // loop through form values and exclude those we've marked to skip
    for (var i = 0; i < values.length; i++) {
      if (-1 !== skip.indexOf(values[i]['name'])) {
        continue;
      }

      data[values[i]['name']] = values[i]['value'];
    }


    request = $.ajax({

      beforeSend: function(jqXHR, settings) {
      },
      url: rcpfb.ajax_url,
      type: 'POST',
      dataType: 'json',
      data: data
    });

    request.done(function (response, textStatus, jqXHR) {
      if (response.result == 'SUCCESS') {
        $('p').removeClass('rcpfb-hide-field');
        $.each(response.data, function (key, value) {
          $('[name=' + value + ']').closest("p").addClass("rcpfb-hide-field");
        });
      }
    });

  }
  
})(jQuery);