<?php

/**
 * The plugin bootstrap file
 * @link              https://figarts.co
 * @since             1.0.0
 * @package           Rcpfb
 *
 * @wordpress-plugin
 * Plugin Name:       Restrict Content Pro - Form Builder
 * Plugin URI:        https://figarts.co
 * Description:       Drag and Drop Form Builder for managing fields in Restrict Content Pro
 * Version:           1.4.3
 * Author:            David Towoju (Figarts)
 * Requires at least: 4.0
 * Tested up to:      4.8
 * Requires PHP:      5.6
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rcpfb
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'RCPFB_VERSION', '1.4.3' );
define( 'RCPFB_DIR', plugin_dir_path( __FILE__ ) );
define( 'RCPFB_DIR_URL', plugin_dir_url( __FILE__ ) );
define( 'RCPFB_BASE', plugin_basename( __FILE__ ) );
define( 'RCPFB_ADMIN_PARTIALS', RCPFB_DIR . 'admin/partials' );

if ( ! class_exists( 'RCPFB_License_Menu' ) ) {
	require_once( RCPFB_DIR . 'includes/class-rcpfb-license.php' );

	RCPFB_License_Menu::instance( __FILE__, 'RCPFB', '1.4.3', 'plugin', 'https://figarts.co/' );
}


/**
 * The code that runs during plugin activation.
 */
function activate_rcpfb() {
  if(!defined('RCP_PLUGIN_VERSION')){
		wp_die( __( 'Please activate Restrict Content Pro before activating the form builder', 'rcpfb' ) );
  }
}

/**
 * If RCP was later deactivated
*/
function rcpfb_auto_deactivate() {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if(is_plugin_inactive('restrict-content-pro/restrict-content-pro.php')) {
		deactivate_plugins( plugin_basename( __FILE__ ) );
	}
}

register_activation_hook( __FILE__, 'activate_rcpfb' );
rcpfb_auto_deactivate();

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rcpfb.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_rcpfb() {

	$plugin = new Rcpfb();
	$plugin->run();

}
run_rcpfb();
