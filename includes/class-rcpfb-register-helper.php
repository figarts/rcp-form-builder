<?php
/**
 * Register Helper
 *
 * @since      1.0.0
 * @package    rcpfb
 * @subpackage rcpfb/includes
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Rcpfb_Register_Fields {
  /**
   * Field
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $plugin_name    The ID of this plugin.
   */
  private $fields;

  /**
   * The ID of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $plugin_name    The ID of this plugin.
   */
  private $plugin_name;

  /**
   * The version of this plugin.
   *
   * @since    1.0.0
   * @access   private
   * @var      string    $version    The current version of this plugin.
   */
  private $version;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string    $plugin_name       The name of this plugin.
   * @param      string    $version    The version of this plugin.
   */
  public function __construct( $fields, $plugin_name, $version ) {
    // wp_dump($fields);

    $this->fields = $fields;
    $this->plugin_name = $plugin_name;
    $this->version = $version;

    add_action( 'rcp_before_register_form_fields', array($this, 'rcpfb_before_register_form') );
    add_action( 'rcp_after_password_registration_field', array($this, 'rcpfb_after_password_registration') );
    add_action( 'rcp_before_subscription_form_fields', array($this, 'rcpfb_before_subscription_form') );
    add_action( 'rcp_after_register_form_fields', array($this, 'rcpfb_after_register_form') );
    add_action( 'rcp_before_registration_submit_field', array($this, 'rcpfb_before_registration_submit') );


    add_action( 'init', array($this, 'rcpfb_rcp_add_profile_fields') );
    add_action( 'init', array($this, 'rcpfb_rcp_add_member_edit_fields') );

    add_action( 'rcp_form_errors', array($this, 'rcpfb_validate_fields_on_register'), 10 );
    add_action( 'rcp_form_processing', array($this, 'rcpfb_save_user_fields_on_register'), 10, 2 );
    add_action( 'rcp_user_profile_updated',  array($this, 'rcpfb_save_user_fields_on_profile_save'), 10 );
    add_action( 'rcp_edit_member',  array($this, 'rcpfb_save_user_fields_on_profile_save'), 10 );
  }

  /**
   * Adds the custom fields to the registration form - before register form
   *
   */
  function rcpfb_before_register_form(){
    // dump($this);
    if(isset($this->fields['before_register_form'])){
      $fields = $this->fields['before_register_form'];
      foreach($fields as $field){
        $this->rcpfb_field_generator($field);
      }
    }
  }

  /**
   * Adds the custom fields to the registration form - after password
   *
   */
  function rcpfb_after_password_registration(){

    if(isset($this->fields['after_password_registration'])){
      $fields = $this->fields['after_password_registration'];
      foreach($fields as $field){
        $this->rcpfb_field_generator($field);
      }
    }
  }


  /**
   * Adds the custom fields to the registration form - after register form
   *
   */
  function rcpfb_after_register_form(){
    if(isset($this->fields['after_register_form'])){
      $fields = $this->fields['after_register_form'];
      foreach($fields as $field){
        $this->rcpfb_field_generator($field);
      }
    }
  }


  /**
   * Adds the custom fields to the registration form - before submit
  *
  */
  function rcpfb_before_registration_submit() {
    if(isset($this->fields['before_registration_submit'])){
      $fields = $this->fields['before_registration_submit'];
      foreach($fields as $field){
        $this->rcpfb_field_generator($field);
      }
    }
  }

  /**
   * Adds the custom fields to the registration form - before subscription
   *
   */
  function rcpfb_before_subscription_form(){
    if(isset($this->fields['before_subscription_form'])){
      $fields = $this->fields['before_subscription_form'];
      foreach($fields as $field){
        $this->rcpfb_field_generator($field);
      }
    }
  }


  /**
   * Adds the custom fields to profile editor
   *
   */
  function rcpfb_rcp_add_profile_fields(){
    foreach($this->fields as $key => $fields){
      add_action( 'rcp_profile_editor_after', array($this, 'rcpfb_'.$key) );
    }
  }


  /**
   * Adds the custom fields to the member edit screen
   *
   */
  function rcpfb_rcp_add_member_edit_fields(){
    foreach($this->fields as $key => $fields){
      add_action( 'rcp_edit_member_after', array($this, 'rcpfb_'.$key) );
    }
  }

  /**
   * Determine whether a field should be displayed based on selected pages
   *
   */
  function rcpfb_field_generator($field){
    global $rcp_options;

    // on the edit profile page
    if ( get_the_ID() == $rcp_options['edit_profile'] ){
      if (in_array($field['pages'],[1,2]) ){
        $this->rcpfb_generate_html($field);
      }
    }
    elseif( is_admin() && current_user_can('manage_options') && ( (isset($_GET['edit_member']) && $_GET['page'] == 'rcp-members') || (isset($_GET['view']) && $_GET['page'] == 'rcp-customers')) ){
    // on the edit member page
      if (in_array($field['pages'],[1,3]) ){
        $this->rcpfb_generate_edit_html($field);
      }
    }
    else{
      $this->rcpfb_generate_html($field);
    }
  }


  /**
   * Generates HTML input fields
   *
   * @since    1.0.0
   * @param      string    $field
   */
  public function rcpfb_generate_html($field){

    $c_level = rcp_get_subscription_id();
    // Exclude fields that do not show up in the selected level
    if ($c_level !== false && isset($field['role'])) {
      $roles = explode(',', $field['role']);
      if ( is_numeric( $c_level ) && array_search( $c_level, $roles ) === false )
      return;
    }

    // Hide from logged in users
    if(isset($field['hidefromloggedin']) && true == $field['hidefromloggedin']){
      if(is_user_logged_in()){
        return;
      }
    }

    extract($field, EXTR_SKIP);

    $name = isset($name) ? $name : '';
    $f_value = get_user_meta( get_current_user_id(), $name, true );
    $value = isset( $f_value ) ? $f_value : '' ;
    $label = isset($label) ? $label : '';
    $subtype = isset($subtype) ? $subtype : '';
    $class = isset($className) ? $className : '';
    $class = isset($required) ? $class.' required' : $class;
    $desc = isset($description) ? $description : '';
    // $size = isset($size) ? $size : '';
    $maxlength = isset($maxlength) ? $maxlength : '';

    $allowed_html = array(
      'a' => array(
          'href' => array(),
          'title' => array()
      ),
      'br' => array(),
      'em' => array(),
      'strong' => array(),
    );
    $label = wp_kses($label, $allowed_html);

    // Text Field
    if($field['type'] == 'text'){
      ?>
      <p>
        <label for="<?php esc_attr_e( $name ) ?>"><?php echo $label; ?></label>
        <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="<?php esc_attr_e( $subtype ) ?>" value="<?php echo esc_attr_e( $value ); ?>" class="<?php esc_attr_e($class) ?>" maxlength="<?php esc_attr_e( $maxlength ) ?>" />
        <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>
      <?php
    }

    // Select Field
    if($field['type'] == 'select'){
      if ( isset($multiple) && $multiple == true ){
        $attr = 'multiple=multiple';
        $class .= ' select2';
        $name = $name.'[]';
        ?>
        <script>
          jQuery(document).ready(function() {
            jQuery('.rcp_form .select2').select2();
          });
        </script>
      <?php
      }

      if ( $field['subtype'] == 'country' ) {
        $field['values'] = rcpfb_get_countries();
      }

      ?>
      <p>
        <label for="<?php esc_attr_e( $name ) ?>"><?php echo $label; ?></label>
        <select id="<?php esc_attr_e( $name ) ?>" name="<?php esc_attr_e( $name ) ?>" class="<?php esc_attr_e($class) ?>" <?php echo isset ($attr) ? esc_attr( $attr ) : '' ?> >
          <?php

          if ( $field['subtype'] == 'country' ) {
            foreach($field['values'] as $key => $option){ ?>
              <option value="<?php esc_attr_e( $key ) ?>" <?php selected( ($value == false && isset($option['selected']) && $option['selected'] == true) ? $key : ( is_array( $value ) && in_array( $key, $value ) ) ? $key : $value, $key) ?>  ><?php echo esc_html( $option ) ?></option>
              <?php
            }
          }
          else{
            foreach($field['values'] as $option){ ?>
              <option value="<?php esc_attr_e( $option['value'] ) ?>" <?php selected( ($value == false && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : ( is_array( $value ) && in_array( $option['value'], $value ) ) ? $option['value'] : $value, $option['value']) ?>  ><?php echo esc_html( $option['label'] ) ?></option>
              <?php
            }
          }
          ?>
        </select>
        <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>
      <?php
    }

    // TextArea Field
    if($field['type'] == 'textarea' && $field['subtype'] != 'html'){
      $rows = isset($rows) ? $rows : '';
      ?>
      <p>
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo $label; ?></label>
          <textarea id="<?php esc_attr_e( $name ) ?>" name="<?php esc_attr_e( $name ) ?>" rows="<?php esc_attr_e( $rows ) ?>" maxlength="<?php esc_attr_e( $maxlength ) ?>" ><?php echo esc_textarea( $value ); ?></textarea>
          <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>

      <?php
    }
    // HTML Field
    if($field['type'] == 'paragraph'){
      $rows = isset($rows) ? $rows : '';
      ?>
      <p>
        <label for=""><?php echo '' ?></label>
        <?php echo htmlspecialchars_decode( wp_kses_post($label) );  ?>
        <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>

      <?php
    }

    // Radio
    if($field['type'] == 'radio-group'){
      ?>
      <p>
        <label for="<?php esc_attr_e( $name ) ?>"><?php echo $label; ?></label>
        <?php foreach($field['values'] as $option) { ?>
          <label for="<?php esc_attr_e( $option['label'] ) ?>">
            <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $option['label'] ) ?>" type="radio" value="<?php esc_attr_e( $option['value'] ) ?>" <?php checked( ($value == '' && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : $value, $option['value'] ); ?>/>
            <?php esc_attr_e( $option['label'] ); ?>
          </label>
          <br/>
        <?php } ?>
          <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>

      <?php
    }

    // Checkbox Field
    if($field['type'] == 'checkbox-group'){
      ?>
      <p> <?php echo esc_html( $label ); ?> </p>

      <p>
        <?php
        foreach($field['values'] as $option) {
          $option['id'] = str_replace(' ', '-', strtolower($option['label']) );
          ?>
          <label for="<?php esc_attr_e( $option['id']) ?>">
            <input name="<?php echo esc_attr( $name.'[]' ) ?>" id="<?php esc_attr_e( $option['id'] ) ?>" type="checkbox" value="<?php echo esc_attr( $option['value'] ) ?>" <?php checked( ($value == '' && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : ( is_array( $value ) && in_array( $option['value'], $value ) ) ? $option['value'] : '', $option['value'] ); ?>/>
            <?php esc_attr_e( $option['label'] ); ?>


          </label>
          <br/>
        <?php } ?>
        <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>
     <?php
    }

    // Date Field
    if($field['type'] == 'date'){
      $class .= ' datepicker';
      ?>
      <p>
        <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="text" value="<?php echo esc_attr_e( $value ); ?>" class="<?php echo esc_attr($class) ?>"/>
        <span class="rcpfb-description"><?php echo $desc; ?></span>
      </p>
      <script>
        jQuery(document).ready(function() {
          jQuery('.datepicker').datepicker({format: 'mm-dd-yyyy'});
        });
      </script>
     <?php
    }

    // Hidden
    if($field['type'] == 'hidden'){
      ?>
        <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="hidden" value="<?php echo isset($field['value']) ? esc_attr($field['value']) : ''; ?>" class="<?php echo esc_attr($class) ?>"/>
      <?php
    }

    // HTML
    if( $field['type'] == 'header' || $field['type'] == 'paragraph' ){
      // echo '<'.$field["subtype"].' class="'.esc_attr($class).'">';
      // echo esc_html( $field['label'] );
      // echo "</".$field['subtype'].">";
    }
  }



  /**
   * Generates HTML input fields for admin
   *
   * @since    1.0.0
   * @param      string    $field
   */
  public function rcpfb_generate_edit_html($field){

    $user_id = ( isset($_GET['edit_member']) && is_numeric($_GET['edit_member'])) ? $_GET['edit_member'] : '0';

    if( RCP_PLUGIN_VERSION >= '3.0' ){
      $customer_id  = ( isset($_GET['customer_id']) && is_numeric($_GET['customer_id'])) ? $_GET['customer_id'] : '0';
      $customer     = rcp_get_customer( $customer_id );
      $user_id      = $customer->get_user_id();
    }


    $c_level = rcp_get_subscription_id($user_id);
    // Exclude fields that do not show up in the selected level
    if ($c_level !== false && isset($field['role'])) {
      $roles = explode(',', $field['role']);
      if ( is_numeric( $c_level ) && array_search( $c_level, $roles ) === false )
      return;
    }

    extract($field);
    $name = isset($name) ? $name : '';
    $$name = get_user_meta(  $user_id, $name, true );
    $value = isset($$name) ? $$name : '' ;
    $label = isset($label) ? $label : '';
    $subtype = isset($subtype) ? $subtype : '';
    $class = isset($className) ? $className : '';
    $class = isset($required) ? $class.' required' : $class;
    // $size = isset($size) ? $size : '';
    $maxlength = isset($maxlength) ? $maxlength : '';

    // Text Field
    if($field['type'] == 'text'){
      ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        </th>
        <td>
        <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="<?php esc_attr_e( $subtype ) ?>" value="<?php echo esc_attr_e( $value ); ?>" class="<?php esc_attr_e($class) ?>"  maxlength="<?php esc_attr_e( $maxlength ) ?>" />
        <p class="description"><?php echo isset($description) ? esc_html( $description) : '' ?></p>
        </td>
      </tr>
      <?php
    }

    // Select Field
    if($field['type'] == 'select'){
      if ( isset($multiple) && $multiple == true ){
        $attr = 'multiple=multiple';
        $class .= ' select2'; ?>
        <script>
          jQuery(document).ready(function() {
            jQuery('#rcp-edit-member .select2').select2();
          });
        </script>
      <?php
      }
      $maybe_multiple_name = (isset($multiple) && $multiple == true ) ? $name.'[]' : $name;
      if ( $field['subtype'] == 'country' ) {
        $field['values'] = rcpfb_get_countries();
      }

      ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php esc_html_e( $label ); ?></label>
        </th>
        <td>
          <select id="<?php esc_attr_e( $name ) ?>" name="<?php esc_attr_e( $maybe_multiple_name ) ?>" class="<?php esc_attr_e($class) ?>" <?php echo esc_attr( $attr ) ?> >
            <?php
            foreach($field['values'] as $key => $option){

              if ( $field['subtype'] == 'country' ) {
              ?>
                <option value="<?php esc_attr_e( $key ) ?>" <?php selected( ($value == false && isset($option['selected']) && $option['selected'] == true) ? $key : ( is_array( $value ) && in_array( $key, $value ) ) ? $key : $value, $key) ?>  ><?php echo esc_html( $option ) ?></option>
              <?php
              }
              else{
              ?>
                <option value="<?php esc_attr_e( $option['value'] ) ?>" <?php selected( ($value == false && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : ( is_array( $value ) && in_array( $option['value'], $value ) ) ? $option['value'] : $value, $option['value']) ?>  ><?php echo esc_html( $option['label'] ) ?></option>
              <?php
              }
            }
            ?>
          </select>
          <p class="description"><?php echo isset($description) ? esc_html( $description) : '' ?></p>
        </td>
      </tr>
      <?php
    }

    // TextArea Field
    if($field['type'] == 'textarea' && $field['label'] != 'HTML'){
      $rows = isset($rows) ? $rows : '';
      $cols = isset($cols) ? $cols : '';
      ?>

      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        </th>
        <td>
          <textarea id="<?php esc_attr_e( $name ) ?>" name="<?php esc_attr_e( $name ) ?>" rows="<?php esc_attr_e( $rows ) ?>" cols="<?php esc_attr_e( $cols ) ?>" maxlength="<?php esc_attr_e( $maxlength ) ?>" ><?php echo esc_textarea( $value ); ?></textarea>
          <p class="description"><?php echo isset($description) ? esc_html( $description) : '' ?></p>
        </td>
      </tr>

    <?php
    }

    // Radio
    if($field['type'] == 'radio-group'){
      ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        </th>
        <td>
          <?php foreach($field['values'] as $option) { ?>
            <label for="<?php esc_attr_e( $option['label'] ) ?>">
            <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $option['label'] ) ?>" type="radio" value="<?php esc_attr_e( $option['value'] ) ?>" <?php checked( ($value == '' && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : $value, $option['value'] ); ?>/>
            <?php esc_attr_e( $option['label'] ); ?>
            </label>
            <p class="description"><?php echo isset($description) ? esc_html( $description) : '' ?></p>
            <br/>
          <?php } ?>
        </td>
      </tr>
      <?php
    }

    // Checkbox Field
    if($field['type'] == 'checkbox-group'){
      ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        </th>
        <td>
        <?php foreach($field['values'] as $option) {
        $option['id'] = str_replace(' ', '-', strtolower($option['label']) );
        ?>
          <label for="<?php esc_attr_e( $option['id']) ?>">
          <input name="<?php echo esc_attr( $name.'[]' ) ?>" id="<?php esc_attr_e( $option['id'] ) ?>" type="checkbox" value="<?php echo esc_attr( $option['value'] ) ?>" <?php checked( ($value == '' && isset($option['selected']) && $option['selected'] == true) ? $option['value'] : ( is_array( $value ) && in_array( $option['value'], $value ) ) ? $option['value'] : '', $option['value'] ); ?>/>
          <?php esc_attr_e( $option['label'] ); ?>
          </label>
          <p class="description"><?php echo isset($description) ? esc_html( $description) : '' ?></p>
          <br/>
        <?php } ?>
        </td>
      </tr>
      <?php
    }

    // Date Field
    if($field['type'] == 'date'){
      // wp_dump($field);
      $class .= ' datepicker';
      ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php echo esc_html( $label ); ?></label>
        </th>
        <td>
          <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="text" value="<?php echo esc_attr_e( $value ); ?>" class="<?php echo esc_attr($class) ?>"/>
        </td>
        <script>
          jQuery(document).ready(function() {
            jQuery('.datepicker').datepicker({format: 'mm-dd-yyyy'});
          });
        </script>
      </tr>
      <?php
    }

    // Hidden
    if($field['type'] == 'hidden'){
      ?>
      <p>
        <input name="<?php esc_attr_e( $name ) ?>" id="<?php esc_attr_e( $name ) ?>" type="hidden" value="<?php echo esc_attr_e( $value ); ?>" class="<?php echo esc_attr($class) ?>"/>
      </p>
      <?php
    }

    // HTML
    if( $field['type'] == 'header' || $field['type'] == 'paragraph' ){  ?>
      <tr valign="top">
        <th scope="row" valign="top">
          <label for="<?php esc_attr_e( $name ) ?>"><?php //echo esc_html( $label ); ?></label>
        </th>
        <td>
        <?php
          echo '<'.$field["subtype"].' class="'.esc_attr($class).'">';
          echo esc_html( $field['label'] );
          echo "</".$field['subtype'].">";
        ?>
        </td>
      </tr>
      <?php
    }
  }



  /**
   * Determines if there are problems with the registration data submitted
   *
   * @since    1.0.0
   * @param      string    $posted
   * @param      string    $user_id
   */
  public function rcpfb_validate_fields_on_register( $posted ) {

    if ( rcp_get_subscription_id() ) {
      return;
    }

    $s_level = sanitize_text_field( $posted['rcp_level'] );

    foreach($this->fields as $fields){
      foreach($fields as $field){

        // Exclude fields that do not show up in the selected level
        $roles = isset($field['role']) ? explode(',', $field['role']) : array();
        if ( is_numeric( $s_level ) && array_search( $s_level, $roles ) === false )
        continue;

        if(isset($field['hidefromloggedin']) && true == $field['hidefromloggedin']){
          if(is_user_logged_in()){
            continue;
          }
        }

        if( $field['type'] == 'select' && !empty($posted[$field['name']]) ){

          $available_choices = array( );

          if ( $field['subtype'] == 'country' ) {
            $available_choices = array_keys( rcpfb_get_countries() );
          }
          else{
            foreach($field['values'] as $option){
              $available_choices[] = $option['value'];
            }
          }
          // wp_dump($available_choices);
          $available_choices = array_filter($available_choices);
          // Add an error message if the submitted option isn't one of our valid choices.
          if ( isset($field['multiple']) && $field['multiple'] == TRUE ){
            if( rcpfb_in_array_any( array_values($posted[$field['name']]), $available_choices ) == false ){
                rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s field cannot be empty', 'rcpfb' ), ucwords($field['label']) ), 'register' );
            }
          }
          else{
            // wp_dump( $posted[$field['name']] );
            if ( ! in_array( $posted[$field['name']], $available_choices ) ) {
                rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s field cannot be empty', 'rcpfb' ), ucwords($field['label']) ), 'register' );
            }
          }
          continue;
        }

        if( $field['type'] == 'checkbox-group' && !empty($posted[$field['name']]) ){
          if ( ! isset( $posted[$field['name']] ) || empty( $posted[$field['name']] ) ) {
            rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s field cannot be empty', 'rcpfb' ), ucwords($field['label']) ), 'register' );
          }
          continue;
        }

        if( $field['type'] == 'date' && !empty($posted[$field['name']]) ){
          if ( ! preg_match ("/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/", $posted[$field['name']]) ){
            rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s Incorrect date format', 'rcpfb' ), ucwords($field['label']) ), 'register' );
          }
          continue;
        }

        if( $field['type'] == 'number' && !empty($posted[$field['name']]) ){
          if ( ! is_numeric( $posted[$field['name']] ) ){
            rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s must be a number', 'rcpfb' ), ucwords($field['label']) ), 'register' );
          }
          continue;
        }

        if( $field['required'] == true ){
          if( empty( trim($posted[$field['name']]) ) ) {
            rcp_errors()->add( 'invalid_'.$field['name'], sprintf( esc_html__( '%s field cannot be empty', 'rcpfb' ), ucwords($field['label']) ), 'register' );
          }
        }

      }
    }
  }


  /**
   * Stores the information submitted during registration
   *
   * @since    1.0.0
   * @param      string    $posted
   * @param      string    $user_id
   */
  public function rcpfb_save_user_fields_on_register( $posted, $user_id ) {

    $s_level = sanitize_text_field( $posted['rcp_level'] );

    foreach($this->fields as $fields){
      foreach($fields as $field){
        if(isset($posted[$field['name']])){

          // Exclude fields that do not show up in the selected level
          $roles = isset($field['role']) ? explode(',', $field['role']) : array();

          if ( is_array($roles) && ($roles) ) {
            if ( is_numeric( $s_level ) && array_search( $s_level, $roles ) === false )
            continue;
          }

          if($field['type'] == 'select'){
            if( isset($field['multiple']) && $field['multiple'] == true ){
              // wp_dump($field);
              if( is_array( $posted[$field['name']] ) && ! empty( $posted[$field['name']] ) ) {
                update_user_meta( $user_id, $field['name'], array_map('sanitize_text_field',$posted[$field['name']]) );
              }
            }
            else{
              if( ! empty( $posted[$field['name']] ) ) {
                update_user_meta( $user_id, $field['name'], sanitize_text_field( $posted[$field['name']] ) );
              }
            }
            continue;
          }

          if($field['type'] == 'checkbox-group'){
            if ( isset( $posted[$field['name']] ) && ! empty( $posted[$field['name']] ) ) {
              update_user_meta( $user_id, $field['name'], array_map('sanitize_text_field',$posted[$field['name']]) );
            }
            continue;
          }

          if($field['type'] == 'date'){
            $date = $posted[$field['name']];
            if ( !empty( $date ) ) {
              update_user_meta( $user_id, $field['name'], $date );
            }
            continue;
          }

          if( ! empty( $posted[$field['name']] ) ) {
            update_user_meta( $user_id, $field['name'], sanitize_text_field( $posted[$field['name']] ) );
          }

        }
      }
    }
  }


  /**
  * Stores the information submitted profile update
   *
   * @since    1.0.0
   * @param      string    $user_id
  */
  public function rcpfb_save_user_fields_on_profile_save( $user_id ) {
    foreach($this->fields as $fields){
      foreach($fields as $field){

        if(isset($_POST[$field['name']])){

          if($field['type'] == 'select'){
            if( isset($field['multiple']) && $field['multiple'] == true ){
              // wp_dump($field);
              if( is_array( $_POST[$field['name']] ) && ! empty( $_POST[$field['name']] ) ) {
                update_user_meta( $user_id, $field['name'], array_map('sanitize_text_field',$_POST[$field['name']]) );
              }
            }
            else{
              if( ! empty( $_POST[$field['name']] ) ) {
                update_user_meta( $user_id, $field['name'], sanitize_text_field( $_POST[$field['name']] ) );
              }
            }
            continue;
          }

          if($field['type'] == 'checkbox-group'){
            if ( isset( $_POST[$field['name']] ) && ! empty( $_POST[$field['name']] ) ) {
              update_user_meta( $user_id, $field['name'], array_map('sanitize_text_field',$_POST[$field['name']]) );
            }
            continue;
          }

          if($field['type'] == 'date'){
            $date = $_POST[$field['name']];
            if ( !empty( $date ) ) {
              update_user_meta( $user_id, $field['name'], $date );
            }
            continue;
          }

          if( ! empty( $_POST[$field['name']] ) ) {
            update_user_meta( $user_id, $field['name'], sanitize_text_field( $_POST[$field['name']] ) );
          }

        }


      }
    }
  }



}