<?php
/**
 * RCP Functions
 *
 * Functions that depend on RCP being fully loaded
 *
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>


 */


/**
 * Get RCP levels
 * 
 * @since    1.0.0
 * @param    string    Level 
 */ 
function rcpfb_get_rcplevels($json = true){
  $levels = rcp_get_subscription_levels('active');
  $output = "''";
  if(!empty($levels) && is_array($levels)){
    $getLevels = array();
    foreach($levels as $level){
      $getLevels[$level->id] = $level->name;
    }
    if ($json == true){
      $output =  json_encode($getLevels, JSON_PRETTY_PRINT);
    }else{
      $output = $getLevels;
    }
  }
  // wp_dump($output);
  return $output;

}