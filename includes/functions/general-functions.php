<?php
/**
 * General Functions
 *
 * @package    Rcpfb
 * @subpackage Rcpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */


/**
  * Converts PHP array to JSON object.
  *
  * @since    1.0.0
  * @param    array    $fields 
  */
 function rcpfb_array_to_object($fields){
  $json = array();
  if ( ! is_array($fields)) {
    return;
  }
  foreach ($fields as $field) {
    $itemObject = new stdClass();
    foreach ($field as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $k => $v) {
          $itemObject->{$key}[$k] = array_map('esc_attr',$v);
        }
      }else {
        $itemObject->{$key} = esc_attr($value);
      }
    }
    array_push($json, $itemObject);
  }
  $json = json_encode($json);

  return $json;
}

/**
  * Default options for RCP Form Builder
  * 
  * @since    1.0.0
  * @param    string    Level 
  */
function rcpfb_default_options( ) {

  $defaults = array(
    'fields'    =>  rcpfb_field_positions(),
    'labels'    =>  array(
      'logged_out_header' => '',
      'logged_in_header' => '',
      'rcp_sub_details_renew' => '',
      'rcp_subscription_message' => '',
      'rcp_submit_registration' => '',
    )
  );
  return apply_filters( 'rcpfb_default_options', $defaults );
}

/**
  * Field Positions
  * 
  * @since    1.0.0
  * @param    string    Level 
  */	
function rcpfb_field_positions( ) {
  
  $field_positions = array(
    array(
      'id' => 'before_register_form',
      "type"=> "number",
      "label"=> esc_html__('Before Register Form','rcpfb'),
      'className' => 'rcpfb-default-field'
    ),
    array(
      'id' => 'after_password_registration', 
      "type"=> "number",
      "label"=> esc_html__('After Password Registration','rcpfb'),
      'className' => 'rcpfb-default-field'
    ),
    array(
      'id' => 'before_subscription_form', 
      "type"=> "number",
      "label"=> esc_html__('Before Subscription Form','rcpfb'),
      'className' => 'rcpfb-default-field'
    ),
    array(
      'id' => 'after_register_form', 
      "type"=> "number",
      "label"=> esc_html__('After Billing Fields','rcpfb'),
      'className' => 'rcpfb-default-field'
    ),
    array(
      "id" => "before_registration_submit", 
      "type"=> "number",
      "label"=> esc_html__('Before Registration Submit','rcpfb'),
      'className' => 'rcpfb-default-field',
      'hook' => 'rcp_before_registration_submit_field'
    ),
  );
  return $field_positions;
}

/**
  * Field Positions
  * 
  * @since    1.0.0
  * @param    string    Level 
  */
function rcpfb_get_filter_hooks(){
  return array(
    'logged_out_header' => 'rcp_registration_header_logged_out',
    'logged_in_header' => 'rcp_registration_header_logged_in',
    'rcp_subscription_message' => 'rcp_registration_choose_subscription',
    'rcp_submit_registration' => 'rcp_registration_register_button',
    'rcp_sub_details_renew' => 'rcp_subscription_details_action_renew'
  );
}



/**
 * Get arrays bewtween two values
 * 
 * @since    1.0.0
 * @param    string    start 
 * @param    string    end 
 * @param    array    array 
 */
function rcpfb_getArraysBetweenNames($start, $end, $fields)
{
  $return = [];
  $foundName1 = false;
  
  if (is_array($fields) && !empty($fields)) {
    foreach ($fields as $key => $item) {
      // if($start == 'before_registration_submit') wp_dump($key);
      if ($foundName1) {
        if ( isset($item['id']) && $item["id"] == $end)
          break;

        $return[$key] = $item;
      } elseif ( isset($item['id']) && $item["id"] == $start) {

        $foundName1 = true;        
      }
    }
	}
  return $return;
}


/**
 * Form Builder Type Attribute Settings
 *
 * @since    1.0.0
 * @param    string    $membership 
 */
function rcpfb_get_type_attr(){
		
		$type_attr = array(
			// 'typeUserAttrs' => array(
				'number' => array(
					'id' => array(
	            'label' => 'ID',
	          'type' => 'text',
					),
				),
				'text' => array(					
          'subtype' => array(
		        'label' => 'Type',
		        'options' => array(
		        	'text' => 'Text',
		        	'password' => 'Password',
		        )
          ),     
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ),                       
        ),       
				'textarea' => array(
					'cols' => array(
	          'label' => 'Columns',
	          'type' => 'number',
          ),
          'subtype' => array(
		        'label' => 'Type',
		        'options' => array(
	          'text' => 'Text Area',
	          'html' => 'HTML',
		        )
          ),
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ),      
        ),
				'select' => array(
          'subtype' => array(
		        'label' => 'Type',
		        'options' => array(
		        	'custom' => 'Custom',
		        	'country' => 'Country',
		        )
          ),    
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ),         				
				),
				'checkbox-group' => array(	
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ), 			
				),
				'radio-group' => array(
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ), 		
				),
				'date' => array(
          'invoice' => array(
            'label' => 'Invoice',
            'type' => 'checkbox'
          ),
          'memberslistcsv' => array(
            'label' => 'Add to CSV',
            'type' => 'checkbox'
          ), 		
				),
				'hidden' => array(
        ),				
				'paragraph' => array(),				
				'header' => array(),				
			// )
		);

		$type_attr['text'] = array_merge($type_attr['text'],rcpfb_default_type_attr());
		$type_attr['textarea'] = array_merge($type_attr['textarea'],rcpfb_default_type_attr());
		$type_attr['select'] = array_merge($type_attr['select'],rcpfb_default_type_attr());
		$type_attr['checkbox-group'] = array_merge($type_attr['checkbox-group'],rcpfb_default_type_attr());
		$type_attr['radio-group'] = array_merge($type_attr['radio-group'],rcpfb_default_type_attr());
		$type_attr['date'] = array_merge($type_attr['date'],rcpfb_default_type_attr());
		$type_attr['hidden'] = array_merge($type_attr['hidden'],rcpfb_default_type_attr());
		$type_attr['paragraph'] = array_merge($type_attr['paragraph'],rcpfb_default_type_attr());
		$type_attr['header'] = array_merge($type_attr['header'],rcpfb_default_type_attr());

		// $type_attr = json_encode($type_attr, JSON_PRETTY_PRINT);
		// return substr($type_attr, 1, -1);
		return $type_attr;

}

/**
 * Form Builder Type Attribute Settings
 *
 * @since    1.0.0
 * @param    string    $membership 
 */
function rcpfb_default_type_attr(){
	$defaults = array(
		'pages' => array(
      'label' => esc_html__('Appear in', 'rcpfb'),
      'options' => array(
      	'1' => esc_html__('All (Register, Edit & Profile) pages', 'rcpfb'),
      	'2' => esc_html__('Register & Profile pages', 'rcpfb'),
      	'3' => esc_html__('Register & Edit pages', 'rcpfb'),
      	'4' => esc_html__('Register page', 'rcpfb')
      ),
    ),

    // 'saveoption' => array(
    //   'label' => esc_html__('Save Value', 'rcpfb'),
    //   'options' => array(
    //   	'user' => esc_html__('Per User', 'rcpfb'),
    //   	'membership' => esc_html__('Per Membership', 'rcpfb'),
    //   ),
    // ),
    
    // TODO: This should not appear for hook items that cannot be seen by loggedinusers
    'hidefromloggedin' => array(
      'label' => esc_html__('Hide from Logged In Users', 'rcpfb'),
      'type' => 'checkbox'
    ),
	);
	return $defaults;
}

/**
 * Advanced in_array()
 *
 * @since    1.0.0
 * @param    string    $needles 
 * @param    string    $haystack 
 */
function rcpfb_in_array_any($needles, $haystack) {
   return !!array_intersect($needles, $haystack);
}

/**
 * Get country fields
 *
 * @since    1.0.1
 */
function rcpfb_get_countries() {

  return array("" => esc_html__("Select Country", 'rcpfb'),"AF" => "Afghanistan","AX" => "Åland Islands","AL" => "Albania","DZ" => "Algeria","AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica","AG" => "Antigua and Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia","AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh","BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda","BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia and Herzegovina","BW" => "Botswana","BV" => "Bouvet Island","BR" => "Brazil","BQ" => "British Antarctic Territory","IO" => "British Indian Ocean Territory","VG" => "British Virgin Islands","BN" => "Brunei","BG" => "Bulgaria","BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada","CT" => "Canton and Enderbury Islands","CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad","CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos [Keeling] Islands","CO" => "Colombia","KM" => "Comoros","CG" => "Congo - Brazzaville","CD" => "Congo - Kinshasa","CK" => "Cook Islands","CR" => "Costa Rica","HR" => "Croatia","CU" => "Cuba","CY" => "Cyprus","CZ" => "Czech Republic","CI" => "Côte d’Ivoire","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica","DO" => "Dominican Republic","NQ" => "Dronning Maud Land","DD" => "East Germany","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea","ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands","FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana","PF" => "French Polynesia","TF" => "French Southern Territories","FQ" => "French Southern and Antarctic Territories","GA" => "Gabon","GM" => "Gambia","GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece","GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala","GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti","HM" => "Heard Island and McDonald Islands","HN" => "Honduras","HK" => "Hong Kong SAR China","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia","IR" => "Iran","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle of Man","IL" => "Israel","IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JT" => "Johnston Island","JO" => "Jordan","KZ" => "Kazakhstan","KE" => "Kenya","KI" => "Kiribati","KW" => "Kuwait","KG" => "Kyrgyzstan","LA" => "Laos","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho","LR" => "Liberia","LY" => "Libya","LI" => "Liechtenstein","LT" => "Lithuania","LU" => "Luxembourg","MO" => "Macau SAR China","MK" => "Macedonia","MG" => "Madagascar","MW" => "Malawi","MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands","MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","FX" => "Metropolitan France","MX" => "Mexico","FM" => "Micronesia","MI" => "Midway Islands","MD" => "Moldova","MC" => "Monaco","MN" => "Mongolia","ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar [Burma]","NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles","NT" => "Neutral Zone","NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria","NU" => "Niue","NF" => "Norfolk Island","KP" => "North Korea","VD" => "North Vietnam","MP" => "Northern Mariana Islands","NO" => "Norway","OM" => "Oman","PC" => "Pacific Islands Trust Territory","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territories","PA" => "Panama","PZ" => "Panama Canal Zone","PG" => "Papua New Guinea","PY" => "Paraguay","YD" => "People's Democratic Republic of Yemen","PE" => "Peru","PH" => "Philippines","PN" => "Pitcairn Islands","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar","RO" => "Romania","RU" => "Russia","RW" => "Rwanda","RE" => "Réunion","BL" => "Saint Barthélemy","SH" => "Saint Helena","KN" => "Saint Kitts and Nevis","LC" => "Saint Lucia","MF" => "Saint Martin","PM" => "Saint Pierre and Miquelon","VC" => "Saint Vincent and the Grenadines","WS" => "Samoa","SM" => "San Marino","SA" => "Saudi Arabia","SN" => "Senegal","RS" => "Serbia","CS" => "Serbia and Montenegro","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia","SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa","GS" => "South Georgia and the South Sandwich Islands","KR" => "South Korea","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan","SR" => "Suriname","SJ" => "Svalbard and Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland","SY" => "Syria","ST" => "São Tomé and Príncipe","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania","TH" => "Thailand","TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad and Tobago","TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks and Caicos Islands","TV" => "Tuvalu","UM" => "U.S. Minor Outlying Islands","PU" => "U.S. Miscellaneous Pacific Islands","VI" => "U.S. Virgin Islands","UG" => "Uganda","UA" => "Ukraine","SU" => "Union of Soviet Socialist Republics","AE" => "United Arab Emirates","GB" => "United Kingdom","US" => "United States","ZZ" => "Unknown or Invalid Region","UY" => "Uruguay","UZ" => "Uzbekistan","VU" => "Vanuatu","VA" => "Vatican City","VE" => "Venezuela","VN" => "Vietnam","WK" => "Wake Island","WF" => "Wallis and Futuna","EH" => "Western Sahara","YE" => "Yemen","ZM" => "Zambia","ZW" => "Zimbabwe",);
}

function rcpfb_formbuilder_languages(){
  return array('ar_TN', 'de_DE', 'en_US', 'es_ES', 'fa_IR', 'fi_FI', 'fr_FR', 'hu_HU', 'it_IT', 'nb_NO', 'nl_NL', 'pl_PL', 'pt_BR', 'ro_RO', 'ru_RU', 'tr_TR', 'vi_VN', 'zh_CN', 'zh_TW');
}
