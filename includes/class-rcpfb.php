<?php

/**
 * The core plugin class.

 * @since      1.0.0
 * @package    Rcpfb
 * @subpackage Rcpfb/includes
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */
class Rcpfb {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rcpfb_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
  protected $version;
  
  /**
	 * Data
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      mixed    $data    Saved data in options table
	 */
  protected $data;

	/**
	 * Define the core functionality of the plugin.
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'RCPFB_VERSION' ) ) {
			$this->version = RCPFB_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rcpfb';
		$this->data = get_option('rcpfb');

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once RCPFB_DIR . 'includes/class-rcpfb-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once RCPFB_DIR . 'includes/class-rcpfb-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once RCPFB_DIR . 'admin/class-rcpfb-admin.php';

		require_once RCPFB_DIR . 'includes/functions/general-functions.php';
		require_once RCPFB_DIR . 'includes/class-rcpfb-register-helper.php';

    /* Load Class. */    
    $this->loader = new Rcpfb_Loader();
    $this->loader->add_action('plugins_loaded', $this, 'load_plugins_loaded_dependencies', 100 );

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rcpfb_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Rcpfb_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Rcpfb_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_submenu', 100 );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'save_settings' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'process_form_export' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'reset_form' );
		$this->loader->add_action( 'rcp_invoice_bill_to', $this, 'add_fields_to_invoice', 10, 2);
		$this->loader->add_filter( 'rcp_export_csv_cols_members', $this, 'add_fields_to_csv');
		$this->loader->add_filter( 'rcp_export_memberships_get_data_row', $this, 'add_data_to_csv', 10, 2);
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
    
    $this->loader->add_action( 'wp_enqueue_scripts', $this, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $this, 'enqueue_scripts' );    

		$this->loader->add_action( 'plugins_loaded', $this, 'prepare_fields', 100 );
		$this->loader->add_action( 'init', $this, 'hookify_labels', 100 );
    
    $this->loader->add_action( 'wp_ajax_rcpfb_toggle_fields', $this, 'rcpfb_toggle_fields', 1);
    $this->loader->add_action( 'wp_ajax_nopriv_rcpfb_toggle_fields', $this, 'rcpfb_toggle_fields', 1 );
    $this->loader->add_action( 'rcp_invoice_bill_to', $this, 'add_fields_to_invoice', 10, 2);
	}

	/**
	 * Files that depend on all plugin files fully loaded
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public function load_plugins_loaded_dependencies() {		
    require_once RCPFB_DIR . 'includes/functions/rcp-functions.php';
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

	  // Load only on edit profile and register pages
    global $rcp_options;
    if (! in_array( get_the_ID(), array($rcp_options['registration_page'], $rcp_options['edit_profile']) )) 
      return;

		wp_enqueue_style( $this->plugin_name, RCPFB_DIR_URL . 'assets/css/rcpfb-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'rcpfb-datepicker-css', RCPFB_DIR_URL . 'assets/css/datepicker.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'rcpfb-select2-css', RCPFB_DIR_URL . 'assets/css/select2.min.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

	  // Load only on edit profile and register pages
    global $rcp_options;
    if (! in_array( get_the_ID(), array($rcp_options['registration_page'], $rcp_options['edit_profile']) )) 
      return;

		wp_enqueue_script( 'rcpfb-datepicker-js', RCPFB_DIR_URL . 'assets/js/datepicker.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'rcpfb-select2-js', RCPFB_DIR_URL . 'assets/js/select2.min.js', array( 'jquery' ), $this->version, true );
    wp_enqueue_script( $this->plugin_name,  RCPFB_DIR_URL . 'assets/js/rcpfb.js', array( 'jquery' ), $this->version, true );
    wp_localize_script( $this->plugin_name, 'rcpfb', array('ajax_url' => admin_url( 'admin-ajax.php' ) ));
	}

	/**
	 * Get fields for each position
	 *
	 * @since    1.0.0
	 */
	public function prepare_fields(){
		$options = $this->data;
		$positions = array_column(rcpfb_field_positions(), 'id');
		$rcp_fields = array();

    foreach ($positions as $key => $position) {
			$next = (isset($positions[$key+1])) ? $positions[$key+1] : $positions[$key];
			// Get all the fields associated with each position
			$get_arrays = rcpfb_getArraysBetweenNames($position, $next, $options['fields'] );
			$rcp_fields[$position] = $get_arrays;
    }

		$this->rcpfb_hookify_fields($rcp_fields);
	}


	/**
	 * Registers each field
	 * 
	 * @param      array    $fields_arr
	 * @since    1.0.0
	 */
	public function rcpfb_hookify_fields($fields_arr){
		if( !is_array($fields_arr)) return;
    $fields_arr = array_filter($fields_arr);
		
    $rcpro_field = new Rcpfb_Register_Fields(
      $fields_arr,						// input name, will also be used as meta key
      $this->plugin_name,						// type of field
      $this->version
    );    
	}


	/**
	 * Apply filters of label hooks
	 *
	 * @since    1.0.0
	 */
	public function hookify_labels(){
    $options = $this->data;
    if (empty($options))
      return;
    
    $labels = array_filter($options['labels']);
    $hooks = rcpfb_get_filter_hooks();
    foreach ($labels as $key => $value) {
      add_filter( $hooks[$key] , function() use( $value) { return $value; } );
    }
  }


  /**
   * Calls the load_fields() method for gateways when a gateway selection is made
   *
   * @since  2.1
   * @return void
   */
  function rcpfb_toggle_fields() {
    $data = get_option('rcpfb');
    $fields = $data['fields'];

    if (empty($fields)) 
      die();

    $names = array();
    foreach ($fields as $field) {

      if ( $field['type'] != 'number' && isset($field['role']) && !empty($field['role']) ) {

        $s_level = sanitize_text_field( $_POST['rcp_level'] );
        $roles = explode(',', $field['role']);
        // wp_dump( array_search( $s_level, $roles ) === false );
        if ( is_numeric( $s_level ) && array_search( $s_level, $roles ) === false ){
          $names[] = $field['name'];
        }
      }
    }

    wp_send_json( array(
      'result' => 'SUCCESS',
      'data' => $names
    ) );

  }

  public function add_fields_to_invoice($rcp_payment, $rcp_member){
    $data = get_option('rcpfb');
    $fields = $data['fields'];
    $rarray = array();

    foreach ($fields as $field) {
      if ( array_key_exists('invoice', $field) && $field['invoice'] == true ) {
        $rarray[] = $field;
      }
    }

    $html = '';
    foreach ($rarray as $field) {
      $label = $field['label'];
      $name = $field['name'];
			$value = get_user_meta( $rcp_member->ID, $name, true );
			if(is_array($value)){
				$value = implode(', ', $value);
			}
      $html .= "<p class='rcpfb-invoice'><span class='label'>" . $label. ':</span> <strong>' . $value . "</strong></p>";
    }
    echo $html;
  }


	public function add_fields_to_csv($cols){
		$rcpfb = get_option('rcpfb');
		$fields = (array) $rcpfb['fields'];

		$labels = array();

		foreach ($fields as $field) {
			if(array_key_exists('memberslistcsv', $field) && true == $field['memberslistcsv']){
				$labels[$field['name']] = $field['label'];
			}
		}

		$cols = array_merge($cols, $labels);

		return $cols;
	}

	public function add_data_to_csv($data, $membership){
		$rcpfb = get_option('rcpfb');
		$fields = (array) $rcpfb['fields'];

		foreach ($fields as $field) {
			if(array_key_exists('memberslistcsv', $field) && true == $field['memberslistcsv']){
				$value = get_user_meta($data['user_id'], $field['name'], true);
				$value = is_array($value) ? implode(', ', $value) : $value;
				$data[$field['name']] = $value;
			}
		}
		return $data;
	}


	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rcpfb_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
