=== Restrict Content Pro - Form Builder ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://figarts.co
Tags: comments, spam
Requires at least: 4.0
Tested up to: 5.0
Stable tag: 1.4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Drag and Drop Form Builder for managing fields in Restrict Content Pro

== Description ==

Drag and Drop Form Builder for managing fields in Restrict Content Pro

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `rcpfb.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

== Changelog ==

= 1.4.3 =
- changed "Form Builder" to "Fields"
- fixed: radio button label showing when field is hidden
- added toggle to show/hide field to logged in users
- updated to 1.4.3

= 1.0 =
* Launch